====
zorg
====


.. image:: https://img.shields.io/pypi/v/zorg.svg
        :target: https://pypi.python.org/pypi/zorg

.. image:: https://img.shields.io/travis/santoshphilip/zorg.svg
        :target: https://travis-ci.com/santoshphilip/zorg

.. image:: https://readthedocs.org/projects/zzorg/badge/?version=latest
        :target: https://zorg.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Library for an active memory surface


* Free software: Mozilla Public License 2.0 (MPL-2.0)
* Documentation: https://zzorg.readthedocs.io.
* Home Page: https://bitbucket.org/santoshphilip/zorg


Features
--------

Develop a simulation of an "Active Memory Surface"

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
