# Copyright (c) 2020 Santosh Philip
# =======================================================================
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# =======================================================================
"""core routines for zorg"""

def makegrid(dimx, dimy):
    """make a grid of size dimx, dimy"""
    return [[0 for j in range(dimx)] for i in range(dimy)]
    
def displaygrid(grid):
    """text display of the grid"""
    lines = []    
    for row in grid:
        line = ' '.join([str(cell) for cell in row])
        lines.append(line)
    return '\n'.join(lines)
    
def activatecell(grid, row, col, val=1):
    """activate a cell in the grid"""
    grid[row][col] = val
    return grid
    
def killcell(grid, row, col):
    """set cell value to 0"""
    return activatecell(grid, row, col, val=0)    

def activategrid(grid, pattern, dct=None):
    """activate the grid with the pattern"""
    nrows = len(grid)
    ncols = len(grid[0])
    pgrid = pattern2grid(pattern, dct=dct)
    for i in range(nrows):
        for j in range(ncols):
            grid[i][j] = pgrid[i][j]
    return grid
    
def pattern2grid(pattern, dct=None):
    """return a grid from the pattern"""
    if not dct:
        dct = {'0':0, 'X':1}
    pattern = pattern.strip()
    return [[dct[cell] for cell in row.strip()] for row in pattern.splitlines()] 
    
def flipcell(cell):
    """flip a cell"""
    changedct = {1:0, 0:1}
    return changedct[cell]
    

def nextgen(grid, func):
    """return the next gen. grid using rules in func"""
    return [[func(cell) for cell in row] for row in grid]       
    

def reversepattern(grid):
    """reverse the pattern in the grid. Assue 1 0 pattern"""
    func = flipcell
    nextgrid = nextgen(grid, func)
    copyintogrid(nextgrid, grid)

    
def copyintogrid(fromgrid, togrid):
    """copy from fromgrid  to togrid"""
    for i, row in enumerate(fromgrid):
        for j, cell, in enumerate(row):
            togrid[i][j] = cell
    

def copygrid(grid):
    """make a copy o the grid"""
    nrows = len(grid)
    ncols = len(grid[0])
    copyofgrid = makegrid(ncols, nrows)
    for i in range(nrows):
        for j in range(ncols):
            copyofgrid[i][j] = grid[i][j]
    return copyofgrid
        
    
# TODO : need to thorw exception when cell is outside grid
# functions
# reversepattern
            
def main():
    dimx, dimy = 6, 4
    grid = makegrid(dimx, dimy)
    print('=== makegrid ===')
    print(displaygrid(grid))
    activatecell(grid, 1, 1)
    activatecell(grid, 2, 2)
    activatecell(grid, 3, 3)
    print('=== activatecell ===')
    print(displaygrid(grid))
    pattern = """
    XX0XXX
    X0X0XX
    XX0X0X
    XXX0XX
    """
    activategrid(grid, pattern)
    print('=== activategrid ===')
    print(displaygrid(grid))
    reversepattern(grid)
    print('=== reversepattern ===')
    print(displaygrid(grid))
    


if __name__ == '__main__':
    main()