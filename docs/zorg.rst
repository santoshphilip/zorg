zorg package
============

Submodules
----------

zorg.core module
----------------

.. automodule:: zorg.core
   :members:
   :undoc-members:
   :show-inheritance:

zorg.zorg module
----------------

.. automodule:: zorg.zorg
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: zorg
   :members:
   :undoc-members:
   :show-inheritance:
