"""py.test for core.py"""

import pytest
from zorg import core

@pytest.mark.parametrize('nrows, ncols, grid', [
    (
    2, 3, 
    [
        [0, 0,], 
        [0, 0,], 
        [0, 0,]
    ]
    ), # nrows, dim, grid
    ])
def test_makegrid(nrows, ncols, grid):
    """py.test for makegrid"""
    result = core.makegrid(nrows, ncols)
    assert result == grid
    
@pytest.mark.parametrize('grid, expected', [
    (
    [
        [0, 0,], 
        [0, 0,], 
        [0, 0,]
    ],
"""0 0
0 0
0 0"""    
    ), # grid, expected
])
def test_displaygrid(grid, expected):
    """py.test for displaygrid"""
    result = core.displaygrid(grid)    
    assert result == expected
    
@pytest.mark.parametrize('grid, row, col, val, expected', [
    (
    [
        [0, 0, 0], [0, 0, 0], [0, 0, 0]
    ], 0, 1, 1, 
    [
        [0, 1, 0], [0, 0, 0], [0, 0, 0]
    ]
    ), # grid, x, y, val, expected
])
def test_activatecell(grid, row, col, val, expected):
    """py.test for activatecell"""
    core.activatecell(grid, row, col, val)
    result = grid
    assert result == expected
    
@pytest.mark.parametrize('grid, row, col, expected', [
    (
    [
        [0, 0, 0], [1, 1, 1], [0, 0, 0]
    ], 1, 0, 
    [
        [0, 0, 0], [0, 1, 1], [0, 0, 0]
    ]
    ), # grid, row, col, expected
])
def test_killcell(grid, row, col, expected):
    """py.test for killcell"""
    core.killcell(grid, row, col)
    result = grid
    assert result == expected

@pytest.mark.parametrize('pattern, expected', [
    (
    """
    000
    X0X   
    00X
    """,
    [
        [0, 0, 0],
        [1, 0, 1],
        [0, 0, 1],
    ],
    ), # pattern, expected
])    
def test_pattern2grid(pattern, expected):
    """py.test for pattern2grid"""
    result = core.pattern2grid(pattern)
    assert result == expected
    
@pytest.mark.parametrize('grid, pattern, expected', [
    (
    [
        [0, 0, 0],
        [1, 0, 1],
        [0, 0, 1],
    ],
    """XXX
    X0X
    0X0""",
    [
        [1, 1, 1],
        [1, 0, 1],
        [0, 1, 0],
    ]
    ) # grid, pattern, expected
])
def test_activategrid(grid, pattern, expected):
    """py.test for activategrid"""
    core.activategrid(grid, pattern)
    result = grid
    assert result == expected
    
@pytest.mark.parametrize('grid, expected', [
    (
    [
        [0, 1],
        [1, 0]
    ],
    [
        [1, 0],
        [0, 1]
    ],
    ), # grid, expected
])
def test_reversepattern(grid, expected):
    """py.test for reversepattern"""
    core.reversepattern(grid)
    result = grid 
    print(result)
    print(expected)   
    assert result == expected
    
@pytest.mark.parametrize('grid, expected', [
    (
    [
        [0, 1],
        [1, 0],
        [1, 1],
    ],
    [
        [0, 1],
        [1, 0],
        [1, 1],
    ],
    ), # grid, expected
])


def test_copygrid(grid, expected):
    """p.test for copygrid"""
    result = core.copygrid(grid)    
    assert result == expected
    
@pytest.mark.parametrize('cell, expected', [
    (0, 1), # cell, expected
])
def test_flipcell(cell, expected):
    """py.test for flipcell"""
    result = core.flipcell(cell)    
    assert result == expected
    

@pytest.mark.parametrize('grid, func, expected', [
    ([[0, 1]], core.flipcell, [[1, 0]]), # grid, func, expected
])
def test_nextgen(grid, func, expected):
    """py.test for nextgen"""
    result = core.nextgen(grid, func)
    assert result == expected
    assert result != grid
    

@pytest.mark.parametrize('fromgrid, togrid, expected', [
    ([[0, 1]], [[1, 1]], [[0, 1]]), # fromgrid, togrid, expected
])
def test_copyintogrid(fromgrid, togrid, expected):
    """py.test for copyintogrid"""
    core.copyintogrid(fromgrid, togrid)
    assert togrid == expected